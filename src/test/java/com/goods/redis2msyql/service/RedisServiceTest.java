package com.goods.redis2msyql.service;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.Test;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class RedisServiceTest {

    @Test
    public void testIncrease() throws Exception {
        RedisService redisService = new RedisService();

        redisService.increase("BUHAO", 2);

    }

    @Test
    public void testPopNeedPush() throws Exception {
        RedisService redisService = new RedisService();
        String s = redisService.popNeedPush();
        System.out.println(s);
    }

    @Test
    public void testGetKey() throws Exception {
        RedisService redisService = new RedisService();
        String nihao = redisService.getKey("nihao");
        System.out.println(nihao);
    }

    private static JedisCluster jc;

    static {
        //只给集群里一个实例就可以
        Set<HostAndPort> jedisClusterNodes = new HashSet<>();

        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setMaxIdle(100);
        poolConfig.setMaxTotal(300);

        jedisClusterNodes.add(new HostAndPort("192.168.0.110", 7000));
        jc = new JedisCluster(jedisClusterNodes,poolConfig);
    }

    @Test
    public void test() {

        Set<HostAndPort> jedisClusterNodes = new HashSet<>();

        jedisClusterNodes.add(new HostAndPort("192.168.0.110", 7000));
        jc = new JedisCluster(jedisClusterNodes);

        for (int i = 0; i < 1000; i++) {
            String key = "key:" + i;
            jc.setex(key, 60 * 60, key);

        }
    }
}