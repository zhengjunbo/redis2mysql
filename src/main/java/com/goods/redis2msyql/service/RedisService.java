package com.goods.redis2msyql.service;

import com.goods.redis2msyql.util.RedisStorage;
import redis.clients.jedis.Jedis;

/**
 * 采用双写策略，但是会写入
 * @author Dan
 * Created by Administrator on 2015/5/21.
 */
public class RedisService {

    private Jedis jedis;
    /**
     *
     * @param key 目标key
     * @param zan 点赞数
     */
    public void increase(String key,Integer zan){
        getJedis().incrBy(key,zan);//直接增加
        getJedis().sadd("need.push",key); //添加需要增加
    }

    /**
     * 需要被添加的一个一个的推送出来
     * @return
     */
    public String popNeedPush(){
        return getJedis().spop("need.push");
    }

    /**
     * 获取对应key的值
     * @param key
     * @return
     */
    public String getKey(String key){
        return getJedis().get(key);
    }

    private Jedis getJedis(){
        if(jedis==null){
            jedis = RedisStorage.getStorage().getPool().getResource();
        }
        return jedis;
    }


}
