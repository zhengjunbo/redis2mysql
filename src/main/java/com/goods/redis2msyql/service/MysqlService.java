package com.goods.redis2msyql.service;

import com.goods.redis2msyql.util.DataSourceHelper;
import jdbchelper.JdbcHelper;
import jdbchelper.QueryResult;

/**
 * @author Dan
 * Created by Administrator on 2015/5/21.
 */
public class MysqlService {
    private JdbcHelper jdbcHelper;

    public MysqlService(){
        jdbcHelper = DataSourceHelper.getJdbcHelper();
    }

    public void saveOrUpdate(String key,String count){
        String query = "select * from member where name = ?";
        QueryResult result = jdbcHelper.query(query, key);
        if(result.next()){//如果有下一个则存在，所以可以直接更新
            String update = "update member set count = ? where name = ?";
            jdbcHelper.execute(update, count, key);
        } else {
            String insert = "insert into member(name,count) values (?,?)";
            jdbcHelper.execute(insert, key,count );
        }
    }

}
