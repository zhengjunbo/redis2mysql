package com.goods.redis2msyql.util;

import javax.sql.DataSource;

import jdbchelper.JdbcHelper;

public class DataSourceHelper {
    private static DataSource dataSource;

	public static JdbcHelper getJdbcHelper() {
        return new jdbchelper.JdbcHelper(getMySQLDataSource());
	}

    /**
     * 默认的结构
     * @return
     */
	synchronized public static DataSource getMySQLDataSource() {
        if (dataSource == null) {
            Configuration cfg = Configuration.getDefault();
            dataSource = new jdbchelper.SimpleDataSource(cfg.getMySQLDriverClass(), cfg.getMySQLJdbcUrl(),
                    cfg.getMySQLUserName(), cfg.getMySQLPassword());
        }
        return dataSource;
    }


}