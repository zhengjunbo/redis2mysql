package com.goods.redis2msyql.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * The configuration reader.
 * 
 * @author guohb
 * 
 */
public class Configuration {
	private Properties prop = new Properties();

	private static Configuration instance = null;

	public static final String SYSTEM_CFG_FILE = "/system.properties";

	private Configuration() {

	}

	private void loadCfg() {
		InputStream ins = Configuration.class.getResourceAsStream(SYSTEM_CFG_FILE);

		try {
			prop.load(ins);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (ins != null) {
				try {
					ins.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	synchronized public static Configuration getDefault() {
		if (instance == null) {
			instance = new Configuration();
			instance.loadCfg();
		}
		return instance;
	}

	public List<String> getSiteList() {
		List<String> sites = new ArrayList<>();
		String siteItem = getCfgItem("service.sites");
		if (siteItem.length() > 0) {
			String[] items = siteItem.split(",");
			for (String site : items) {
				sites.add(site.trim());
			}
		}
		return sites;
	}

	public String getActiveMQUrl() {
		return getCfgItem("activeMQ.url");
	}

	public String getActiveMQUsername() {
		return getCfgItem("activeMQ.username");
	}

	public String getActiveMQPassword() {
		return getCfgItem("activeMQ.password");
	}

	public String getMySQLDriverClass() {
		return getCfgItem("mysql.jdbc.driverClassName");
	}

	public String getMySQLJdbcUrl() {
		return getCfgItem("mysql.jdbc.url");
	}

	public String getMySQLUserName() {
		return getCfgItem("mysql.jdbc.username");
	}

	public String getMySQLPassword() {
		return getCfgItem("mysql.jdbc.password");
	}

	public String getSecrectKey() {
		return getCfgItem("data.encrypt.seckey");
	}

	public String getServiceEnv() {
		return getCfgItem("service.env");
	}

	public String getAddEmailAccountUrl() {
		return getCfgItem("email.addAccount.url");
	}

	private String getCfgItem(String key) {
		return prop.getProperty(key, "");
	}

}
